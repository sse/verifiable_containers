//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//

#pragma once

#include "./vbst.hpp"
#include "./valued_vbst.hpp"
#include "./utils.hpp"

#include <array>
#include <list>
#include <memory>

namespace sse {
    
    namespace verifiable_containers{
        
        template <class Key, class T, class HashKeyGen = hash_key_gen<Key>, class ValueToString = value_to_string<T> >
        class verifiable_map {
        public:
            struct lookup_proof
            {
                std::list<std::unique_ptr<VBSTAuthElement> > auth_ladder;
                std::array<unsigned char, 32> last_hash;
                
                std::string small_node_key;
                std::string large_node_key;
                
                bool found;
                std::unique_ptr<T> value;
            };
            
            struct insertion_proof
            {
                std::list<std::unique_ptr<VBSTAuthElement> > auth_ladder;
                std::array<unsigned char, 32> last_hash;
                
                std::string small_node_key;
                std::string large_node_key;
                
                bool already_in_set;
                T value;
            };
            
            struct deletion_proof
            {
                std::list<std::unique_ptr<VBSTAuthElement> > key_node_auth_ladder;
                std::array<unsigned char, 32> key_node_last_hash;
                
                std::list<std::unique_ptr<VBSTAuthElement> > aux_auth_ladder;
                std::array<unsigned char, 32> aux_last_hash;
                
                std::string small_node_key;
                std::string large_node_key;
                
                bool in_set;
            };
            
            typedef Key                 key_type;
            typedef T                   mapped_type;
            typedef std::pair<const key_type, const mapped_type>
                                        value_type;
            typedef std::string         digest_type;
            typedef digest_type&        digest_reference;
            typedef HashKeyGen          hasher;
            typedef ValueToString       to_string;
            typedef lookup_proof        lookup_proof_type;
            typedef insertion_proof     insertion_proof_type;
            typedef deletion_proof      deletion_proof_type;
            
            
            verifiable_map() : root_(NULL)
            {
            }
            
            ~verifiable_map()
            {
                if (root_ != NULL) {
                    delete root_;
                }
            }
            
            template <class InputIterator>
            verifiable_map(InputIterator first,
                           InputIterator last,
                           const hasher& hf = hasher())
            : root_(new VBSTValuedNode<T, to_string>(hf(first->first), first->second))
            {
                auto it = first;
                std::advance(it, 1);
                for (; it != last; ++it) {
                    root_->insert_node_key_value(hf(it->first), it->second, false);
                }
				refresh_digest();
            }
            
			void lazy_insert(const key_type& elt, const mapped_type& v,
                                            const hasher& hf = hasher())
            {
                std::string key = hf(elt);
                VBSTValuedNode<T, to_string> *n = new VBSTValuedNode<T, to_string>(key,v);
	            if(root_ != NULL){
                
	                root_->insert_node(n, false);
	            }else{
	                root_ = n;
	            }
	        }
		
			void refresh_digest()
			{
	            if(root_ != NULL){
					root_->refresh_hashes();
				}
			}
			
            digest_type digest() const
            {
                if (root_ == NULL) { // the set is empty, return an empty string
                    return std::string();
                }else{
                    return std::string((char *)root_->get_hash(),32);
                }
            }
            
			bool retrieve(const key_type& elt, mapped_type& v,
                                           const hasher& hf = hasher()) const
			{
				VBSTValuedNode<mapped_type, to_string>* node = (VBSTValuedNode<T, to_string>*)root_->search_node(hf(elt));
				
				if(node == NULL)
				{
					return false;
				}
				
				v = node->get_value();
					
				return true;
			}
 
 
            lookup_proof_type get(const key_type& elt,
                                           const hasher& hf = hasher()) const
            {
                lookup_proof_type proof;
                
                std::string key = hf(elt);
                
                proof.found = root_->search_node_authenticate(key,
                                                              proof.value,
                                                              proof.small_node_key,
                                                              proof.large_node_key,
                                                              proof.auth_ladder,
                                                              proof.last_hash.data());
                
                return proof;
            }
			           
            // the next two methods are similar with a slight difference: the first one raises
            // exceptions while the second one does not
            // this is useful is yourself rely on exception for error management
            static inline void check_lookup_proof_except(const digest_type& digest,
                                                             const key_type& elt,
                                                             const lookup_proof_type& proof,
                                                             const hasher& hf = hasher())
            {
                std::string key = hf(elt);
                VBSTValuedNode<T, to_string>::check_proof(key,
                                                          proof.value.get(),
                                                          proof.found,
                                                          (unsigned char*)digest.data(),
                                                          proof.small_node_key,
                                                          proof.large_node_key,
                                                          proof.auth_ladder,
                                                          proof.last_hash.data());
            }
            
            static bool check_lookup_proof(const digest_type& digest,
                                               const key_type& elt,
                                               const lookup_proof_type& proof,
                                               const hasher& hf = hasher())
            {
                try{
                    check_lookup_proof_except(digest, elt, proof, hf);
                    return true;
                }catch(std::exception const& e){
                    return false;
                }
            }
            
            
            inline insertion_proof_type set(const key_type& elt, const mapped_type& v,
                                            const hasher& hf = hasher())
            {
                insertion_proof_type proof;
                std::string key = hf(elt);
                VBSTValuedNode<T, to_string> *n = new VBSTValuedNode<T, to_string>(key,v);

                if(root_ != NULL){
                    root_->insert_node_authenticate(n, proof.small_node_key, proof.large_node_key, proof.auth_ladder, proof.already_in_set, proof.last_hash.data());
                }else{
                    root_ = n;
                }
                
                proof.value = v;
                
                return proof;
            }
            
            static inline void check_insertion_proof_except(const digest_type& digest,
                                                            const key_type& elt,
                                                            const insertion_proof_type& proof,
                                                            const hasher& hf = hasher())
            {
                std::string key = hf(elt);
                if(!proof.already_in_set){
                    VBSTNode::check_insertion_proof(key, (unsigned char*)digest.data(), proof.small_node_key, proof.large_node_key, proof.auth_ladder);
                }else{
                    // there is no need to check that the previous matching value was correct or not
                    // call VBSTNode verification procedure instead of VBSTValuedNode
                    VBSTNode::check_proof(key, true, (unsigned char*)digest.data(), proof.small_node_key,proof.large_node_key, proof.auth_ladder, proof.last_hash.data());
                }
            }
            
            static bool check_insertion_proof(const digest_type& digest,
                                              const key_type& elt,
                                              const insertion_proof_type& proof,
                                              const hasher& hf = hasher())
            {
                std::string key = hf(elt);
                try{
                    check_insertion_proof_except(digest, elt, proof, hf);
                    return true;
                }catch(std::exception const& e){
                    return false;
                }
            }
            
            
            static digest_type update_digest_addition(digest_type &digest,
                                                      const key_type& elt,
                                                      const mapped_type& v,
                                                      insertion_proof_type& proof,
                                                      const hasher& hf = hasher())
            {
                // modify the auth ladder, and put the value in place of the former string_value
                
                std::string key = hf(elt);
                if(digest.length() != 32)
                {
                    digest.resize(32);
                }
                
                VBSTValuedNode<T, to_string>::recompute_root_hash_insertion(proof.auth_ladder, key, v, proof.already_in_set,(unsigned char*)proof.last_hash.data(), (unsigned char*)digest.data());
                return digest;
            }
            
            static inline digest_type check_update_addition(digest_type &digest,
                                                            const key_type& elt,
                                                            const mapped_type& v,
                                                            insertion_proof_type& proof,
                                                            const hasher& hf = hasher())
            {
                check_insertion_proof_except(digest, elt, proof, hf); // this will raise an exception if the proof is invalid
                return update_digest_addition(digest, elt, v, proof, hf); // update and return the digest
            }
        private:
            VBSTValuedNode<T, to_string> *root_;
        };
        
	} // namespace verifiable_container
} // namespace sse
