//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//

#pragma once

#include "./vbst.hpp"
#include "./utils.hpp"

#include <array>
#include <list>
#include <memory>

namespace sse {

namespace verifiable_containers{
    
    template <class T, class HashKeyGen = hash_key_gen<T> >
    class verifiable_set {
    public:
        struct membership_proof
        {
            std::list<std::unique_ptr<VBSTAuthElement> > auth_ladder;
            std::array<unsigned char, 32> last_hash;
            
            std::string small_node_key;
            std::string large_node_key;
            
            bool found;
        };

        struct insertion_proof
        {
            std::list<std::unique_ptr<VBSTAuthElement> > auth_ladder;
            std::array<unsigned char, 32> last_hash;
            
            std::string small_node_key;
            std::string large_node_key;
            
            bool already_in_set;
        };

        struct deletion_proof
        {
            std::list<std::unique_ptr<VBSTAuthElement> > key_node_auth_ladder;
			std::array<unsigned char, 32> key_node_last_hash;

            std::list<std::unique_ptr<VBSTAuthElement> > aux_auth_ladder;
            std::array<unsigned char, 32> aux_last_hash;
			
            std::string small_node_key;
            std::string large_node_key;
            
            bool in_set;
        };

        typedef T                   key_type;
        typedef key_type            value_type;
        typedef value_type&         reference;
        typedef const value_type&   const_reference;
        typedef std::string         digest_type;
        typedef digest_type&        digest_reference;
        typedef HashKeyGen          hasher;
        typedef membership_proof    membership_proof_type;
        typedef insertion_proof     insertion_proof_type;
        typedef deletion_proof      deletion_proof_type;
        
        
        verifiable_set() : root_(NULL)
        {
        }
        
        ~verifiable_set()
        {
            if (root_ != NULL) {
                delete root_;
            }
        }
        
        template <class InputIterator>
        verifiable_set(InputIterator first,
                       InputIterator last,
                       const hasher& hf = hasher())
        : root_(new VBSTNode(hf(*first)))
        {
            static_assert((std::is_same<value_type, typename InputIterator::value_type>::value),
                          "Invalid iterator::value_type");
            for (auto it = first+1; it != last; it++) {
                root_->insert_node_key(hf(*it), false);
            }
			refresh_digest();
        }
        
		void lazy_insert(const value_type& elt,
                                    const hasher& hf = hasher())
        {
            std::string key = hf(elt);
            if(root_ != NULL){
                
                root_->insert_node_key(key, false);
            }else{
                root_ = new VBSTNode(key);
            }
        }
		
		void refresh_digest()
		{
            if(root_ != NULL){
				root_->refresh_hashes();
			}
		}
		
        digest_type digest() const
        {
            if (root_ == NULL) { // the set is empty, return an empty string
                return std::string();
            }else{
                return std::string((char *)root_->get_hash(),32);
            }
        }
        
    
        membership_proof_type contains(const value_type& elt,
                                       const hasher& hf = hasher()) const
        {
            membership_proof_type proof;
            
            std::string key = hf(elt);
            
            proof.found = root_->search_node_authenticate(key,
                                                          proof.small_node_key,
                                                          proof.large_node_key,
                                                          proof.auth_ladder,
                                                          proof.last_hash.data());
            
            return proof;
        }
        
        // the next two methods are similar with a slight difference: the first one raises
        // exceptions while the second one does not
        // this is useful is yourself rely on exception for error management
        static inline void check_membership_proof_except(const digest_type& digest,
                                                         const value_type& elt,
                                                         const membership_proof_type& proof,
                                                         const hasher& hf = hasher())
        {
            std::string key = hf(elt);
            VBSTNode::check_proof(key,
                                  proof.found,
                                  (unsigned char*)digest.data(),
                                  proof.small_node_key,
                                  proof.large_node_key,
                                  proof.auth_ladder,
                                  proof.last_hash.data());
        }

        static bool check_membership_proof(const digest_type& digest,
                                           const value_type& elt,
                                           const membership_proof_type& proof,
                                           const hasher& hf = hasher())
        {
            try{
                check_membership_proof_except(digest, elt, proof, hf);
                return true;
            }catch(std::exception const& e){
                return false;
            }
        }
        
    
        inline insertion_proof_type add(const value_type& elt,
                                    const hasher& hf = hasher())
        {
            insertion_proof_type proof;
            std::string key = hf(elt);
            if(root_ != NULL){
                
                root_->insert_node_authenticate(key,proof.small_node_key, proof.large_node_key, proof.auth_ladder, proof.already_in_set, proof.last_hash.data());
            }else{
                root_ = new VBSTNode(key);
            }
            return proof;
        }
        
        static inline void check_insertion_proof_except(const digest_type& digest,
                                                        const value_type& elt,
                                                        const insertion_proof_type& proof,
                                                        const hasher& hf = hasher())
        {
            std::string key = hf(elt);
            if(!proof.already_in_set){
                VBSTNode::check_insertion_proof(key, (unsigned char*)digest.data(), proof.small_node_key, proof.large_node_key, proof.auth_ladder);
            }else{
                VBSTNode::check_proof(key, true, (unsigned char*)digest.data(), proof.small_node_key,proof.large_node_key, proof.auth_ladder, proof.last_hash.data());
            }
        }

        static bool check_insertion_proof(const digest_type& digest,
                                          const value_type& elt,
                                          const insertion_proof_type& proof,
                                          const hasher& hf = hasher())
        {
            std::string key = hf(elt);
            try{
                check_insertion_proof_except(digest, elt, proof, hf);
                return true;
            }catch(std::exception const& e){
                return false;
            }
        }


        static digest_type update_digest_addition(digest_type &digest,
                                                  const value_type& elt,
                                                  insertion_proof_type& proof,
                                                  const hasher& hf = hasher())
        {
            if(proof.already_in_set)
            {
                return digest;
            }
            
            std::string key = hf(elt);
            if(digest.length() != 32)
            {
                digest.resize(32);
            }
            
            VBSTNode::recompute_root_hash_insertion(proof.auth_ladder, key, (unsigned char*)digest.data());
            return digest;
        }
        
        static inline digest_type check_update_addition(digest_type &digest,
                                                         const value_type& elt,
                                                         insertion_proof_type& proof,
                                                         const hasher& hf = hasher())
        {
            check_insertion_proof_except(digest, elt, proof, hf); // this will raise an exception if the proof is invalid
            return update_digest_addition(digest, elt, proof, hf); // update and return the digest
        }
        
        inline deletion_proof_type remove(const value_type& elt,
                                          const hasher& hf = hasher())
        {
            deletion_proof_type proof;
            std::string key = hf(elt);
            
            proof.in_set = root_->delete_node_authenticate(key, &root_,
                                                    proof.small_node_key,
                                                    proof.large_node_key,
                                                    proof.key_node_auth_ladder,
                                                    proof.key_node_last_hash.data(),
                                                    proof.aux_auth_ladder,
                                                    proof.aux_last_hash.data());
            return proof;
        }

        static void check_deletion_proof_except(const digest_type& digest,
                                                const value_type& elt,
                                                const deletion_proof_type& proof,
                                                const hasher& hf = hasher())
        {
            std::string key = hf(elt);
            VBSTNode::check_deletion_proof(key, proof.in_set, (unsigned char *)digest.data(), proof.small_node_key, proof.large_node_key, proof.key_node_auth_ladder, proof.key_node_last_hash.data(), proof.aux_auth_ladder, proof.aux_last_hash.data());

        }
        static bool check_deletion_proof(const digest_type& digest,
                                         const value_type& elt,
                                         const deletion_proof_type& proof,
                                         const hasher& hf = hasher())
        {
            std::string key = hf(elt);
            try{
                check_deletion_proof_except(digest, elt, proof, hf);
                return true;
            }catch(std::exception const& e){
                return false;
            }
        }
        
        // To update the local copy of the digest.
        // You should call this function ONLY when the proof has been verified.
        // BE CAREFUL: this function destroys the proof.
        static digest_type update_digest_deletion(digest_type &digest,
                                                  deletion_proof_type& proof)
        {
            if(proof.in_set){
                VBSTNode::recompute_root_hash_deletion(proof.small_node_key, proof.large_node_key, proof.key_node_auth_ladder, proof.key_node_last_hash.data(), proof.aux_auth_ladder, proof.aux_last_hash.data(), (unsigned char*)digest.data());
            }
            return digest;
        }
        
        // Both checks the proof and update the digest.
        // An exception is raised if the proof verification fails.
        // BE CAREFUL: this function destroys the proof.
        static digest_type check_update_deletion(digest_type &digest,
                                                 const value_type& elt,
                                                 deletion_proof_type& proof,
                                                 const hasher& hf = hasher())
        {
            check_deletion_proof_except(digest, elt, proof, hf); // this will raise an exception if the proof is invalid
            return update_digest_deletion(digest, proof); // update and return the digest
        }
        

    private:
        VBSTNode *root_;
    };

} // namespace verifiable_container
} // namespace sse

