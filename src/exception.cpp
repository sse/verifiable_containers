//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//

#include "exception.hpp"

namespace sse {

namespace verifiable_containers{

InvalidProofException::InvalidProofException(const InvalidProofType &t, const std::string &m, const IPUserInfoType &ui): type_(t), message_(m), user_info_(ui)
{
	
}

const char* InvalidProofException::what() const throw()
{
	return message_.c_str();
}

InvalidProofException::~InvalidProofException()
{}
	
	
InvalidProofType InvalidProofException::get_type() const
{
	return type_;
}

const std::string& InvalidProofException::get_message() const
{
	return message_;
}

const IPUserInfoType& InvalidProofException::get_user_info() const
{
	return user_info_;
}

} // namespace verifiable_container
} // namespace sse
