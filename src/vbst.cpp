//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//

#include "vbst.hpp"
#include "exception.hpp"

#include <sse/crypto/hash.hpp>

#include <cassert>
#include <string>

using namespace std;

namespace sse {
namespace verifiable_containers {
        

void print_hash(const unsigned char h[32])
{
	cout << "0x";
	for (int i = 0; i < 32; ++i)
	{
		cout << hex << setw(2) << setfill('0') << (uint)h[i];
	}
	cout << dec;
}
void print_key(const unsigned char h[16])
{
	cout << "0x";
	for (int i = 0; i < 16; ++i)
	{
		cout << hex << setw(2) << setfill('0') << (uint)h[i];
	}
	cout << dec;
}

void print_128(const unsigned char h[128])
{
	cout << "0x";
	for (int i = 0; i < 128; ++i)
	{
		cout << hex << setw(2) << setfill('0') << (uint)h[i];
	}
	cout << dec;
}

void print_hex(const string& s)
{
	cout << "0x";
    for (unsigned char c : s){
        cout << hex << setw(2) << setfill('0') << (uint16_t)c;
    }
	cout << dec;
}

int char_cmp(const unsigned char *v1, const unsigned char *v2, size_t n)
{
	for (unsigned int i = 0; i < n; ++i)
	{
		if(v1[i] < v2[i])
			return -1;
		if(v1[i] > v2[i])
			return 1;
	}
	return 0;
}

void VBSTAuthElement::compute_hash(unsigned char const child_hash [32], unsigned char hash [32]) const
{
//    std::cout << "VBSTAuthElement compute hash\n";

    unsigned char current_hash [32];
    std::string buffer, out;
    
    buffer.reserve(2*32+1+key.size());
    
    memcpy(current_hash,child_hash,32);
    
//    hash_init(&context);
    // cout << "Hash for key "; print_key(cur.key); cout << endl;
    
    if(selected_branch == RIGHT){ // the given opposite hash if for the right branch
        buffer.append((char *)child_hash, 32);
        // print_hash(current_hash);
    }else{
        buffer.append((char *)opp_hash, 32);
        // print_hash(cur.opp_hash);
    }
    // cout << endl;
    
    buffer.append(key);

    if(selected_branch == RIGHT){ // the given opposite hash if for the right branch
        buffer.append((char *)opp_hash, 32);
        // print_hash(cur.opp_hash);
    }else{
        buffer.append((char *)child_hash, 32);
        // print_hash(current_hash);
    }
    // cout << endl;
    
    
    // cout << "Mask: " << hex << setw(2) << setfill('0') << (uint)cur.mask << endl;
    buffer.append((char *)&mask, 1);
    
    sse::crypto::Hash::hash(buffer, out);
    memcpy(hash, out.data(), 32);

    // print_hash(hash); cout <<"\n\n"<< endl;
    
}

void VBSTAuthElement::copy_content(const VBSTAuthElement* e)
{
    key = e->key;
}


VBSTNode::VBSTNode(const string &key): key_(key), c_left_(NULL),  c_right_(NULL)
{
	__recompute_hash();
}

VBSTNode::~VBSTNode()
{
	if( c_left_ != NULL){
		delete  c_left_;
		 c_left_ = NULL;
	}

	if( c_right_ != NULL){
		delete  c_right_;
		 c_right_ = NULL;
	}
}

VBSTNode_const_ptr VBSTNode::search_node(const string &key) const
{
	bool found;
	VBSTNode_const_ptr result;
	
	found = search_node(key, &result, &result);
	
	if(found)
		return result;
	
	return NULL;	
}


bool VBSTNode::search_node(const string &key, VBSTNode_const_ptr* small_node, VBSTNode_const_ptr* large_node) const
{
//    int cmp = char_cmp(get_key(),key,16);
    int cmp = get_key().compare(key);
	if(cmp == 0){
		*small_node = this;
		*large_node = this;
		return true;
	}
	
	if(cmp < 0){ //key_ < key
		*small_node = this;
		
		if( c_left_ == NULL){
			return false;
		}
		return  c_left_->search_node(key, small_node, large_node);
	}
	
	// key >key_
	*large_node = this;
	if( c_right_ == NULL){
		return false;
	}
	return  c_right_->search_node(key, small_node, large_node);
}

bool VBSTNode::search_node_previous(const string &key, VBSTNode_const_ptr* result, VBSTNode_const_ptr* previous) const
{
    int cmp = get_key().compare(key);
	if(cmp == 0){
		*result = this;
		return true;
	}
	
	if(cmp < 0){ //key_ < key
		
		if( c_left_ == NULL){
			return false;
		}
		*previous = this;
		*result =  c_left_;
		return  c_left_->search_node_previous(key, result, previous);
	}
	
	// key >key_
	if( c_right_ == NULL){
		return false;
	}
	*previous = this;
	*result =  c_right_;
	
	return  c_right_->search_node_previous(key, result, previous);
}

VBSTAuthElement* VBSTNode::create_auth_element(bool selected_branch) const
{
    if (selected_branch == RIGHT) { // right branch
        if( c_right_ == NULL){
            return new VBSTAuthElement(get_key(),NULL,true, get_mask());
        }else{
            return new VBSTAuthElement(get_key(), c_right_->get_hash(),true, get_mask());
        }
    }else{
        if( c_left_ == NULL){
            return new VBSTAuthElement(get_key(),NULL,false, get_mask());
        }else{
            return new VBSTAuthElement(get_key(), c_left_->get_hash(),false, get_mask());
        }
    }

}

bool VBSTNode::aux_search_node_authenticate(const string &key, VBSTNode_const_ptr* small_node, VBSTNode_const_ptr * large_node, list<std::unique_ptr<VBSTAuthElement> > &auth, unsigned char const*  last_hash[32]) const
{
	*small_node = NULL;
	*large_node = NULL;
	// __uint128_t s_key = 0, l_key = ~0;

    int cmp = get_key().compare(key);
	if(cmp == 0)
	{
		*small_node = this;
		*large_node = this;
		*last_hash = hash_;
		
		
        auth.push_back(std::unique_ptr<VBSTAuthElement>(create_auth_element(RIGHT)));
        
		if( c_left_ == NULL){
			*last_hash = NULL;
		}else{
			*last_hash =  c_left_->get_hash();
		}
		
		return true;
	}
	
	if(cmp < 0){ //key_ < key
		*small_node = this;
		
        auth.push_back(std::unique_ptr<VBSTAuthElement>(create_auth_element(RIGHT)));

		
		if( c_left_ == NULL){
			*last_hash = NULL;
			return false;
		}
		return  c_left_->aux_search_node_authenticate(key, small_node, large_node, auth, last_hash);
	}
	
	//key_ > key
	
	*large_node = this;
		
    auth.push_back(std::unique_ptr<VBSTAuthElement>(create_auth_element(LEFT)));

	if( c_right_ == NULL){
		*last_hash = NULL;
		return false;
	}
	return  c_right_->aux_search_node_authenticate(key, small_node, large_node, auth, last_hash);
}

bool VBSTNode::search_node_authenticate(const string &key, string& small_node_key, string& large_node_key, list<std::unique_ptr<VBSTAuthElement> > &auth, unsigned char last_hash[32]) const
//bool VBSTNode::aux_search_node_authenticate(const string &key, string& small_node_key, string& large_node_key, list<std::unique_ptr<VBSTAuthElement> > &auth, unsigned char const*  last_hash[32]) const
{
    small_node_key.clear();
    large_node_key.clear();

    VBSTNode_const_ptr small_node, large_node;
    const unsigned char *tmp_hash;
	bool found;
	
	found = aux_search_node_authenticate(key,&small_node,&large_node,auth,&tmp_hash);
    if(small_node != NULL){
        small_node_key = small_node->get_key();
    }
    if(large_node != NULL){
        large_node_key = large_node->get_key();
    }
    
    if(tmp_hash != NULL){
        memcpy(last_hash, tmp_hash,32);
    }else{
        memset(last_hash,0x00,32);
    }

	return found;
}

VBSTNode* VBSTNode::insert_node_key(const string &key, bool recompute_hash )
{
    VBSTNode *n = new VBSTNode(key);
    return insert_node(n, recompute_hash);
}

VBSTNode* VBSTNode::insert_node(VBSTNode *n, bool recompute_hash)
{
    int cmp = get_key().compare(n->get_key());
    if(cmp == 0)
    {
        // insertion means replacement of the node with the same key
        // so for now, we copy the contnent of n into this.
        // for the set structure, this does not change anything,
        // but for valued nodes, it implies replacement of the stored value;
        
        copy_content(n);
        
        // also delete n
        delete n;
        
		if(recompute_hash)
	        __recompute_hash();

        return this;
    }
    
    VBSTNode *inserted_node = NULL;
    
    if(cmp < 0){
        if( c_left_ == NULL){
             c_left_ = n;
            inserted_node =  c_left_;
        }else{
            inserted_node =  c_left_->insert_node(n, recompute_hash);
        }
    }else{
        if( c_right_ == NULL){
             c_right_ = n;
            inserted_node =  c_right_;
        }else{
            inserted_node =  c_right_->insert_node(n, recompute_hash);
        }
    }
    
	if(recompute_hash)
	    __recompute_hash();
    
    return inserted_node;
}

VBSTNode* VBSTNode::insert_node_authenticate(VBSTNode *n, string& small_node_key, string& large_node_key, list<std::unique_ptr<VBSTAuthElement> > &auth, bool &already_existed, unsigned char last_hash [32])
{
    bool flag;
    flag = search_node_authenticate(n->get_key(), small_node_key, large_node_key, auth, last_hash);
    already_existed = flag;
    return insert_node(n);
}

VBSTNode* VBSTNode::insert_node_authenticate(const string& key, string& small_node_key, string& large_node_key, list<std::unique_ptr<VBSTAuthElement> > &auth, bool &already_existed, unsigned char last_hash [32])
{
    VBSTNode *n = new VBSTNode(key);
    return insert_node_authenticate(n, small_node_key, large_node_key, auth, already_existed, last_hash);
}

VBSTNode* VBSTNode::get_left()
{
	return  c_left_;
}
void VBSTNode::set_left(VBSTNode* node)
{
	 c_left_ = node;
}

VBSTNode* VBSTNode::get_right()
{
	return  c_right_;
}
void VBSTNode::set_right(VBSTNode* node)
{
	 c_right_ = node;
}

unsigned char VBSTNode::get_mask() const
{
	unsigned char m = 0;
	if ( c_left_ == NULL)
	{
		m ^= 0x10;
	}
	if ( c_right_ == NULL)
	{
		m ^= 0x01;
	}
	return m;
}

const string& VBSTNode::get_key() const
{
	return key_;
}
const unsigned char* VBSTNode::get_hash() const
{
	return hash_;
}

void VBSTNode::__recompute_hash()
{
    std::string buffer, out;
    buffer.reserve(2*32+1+get_key().size());

	unsigned char mask = 0;
	// cout << "Internal hash for key "; print_key(key_); cout << endl;
	
	if( c_left_ != NULL){
        buffer.append((char *) c_left_->get_hash(),32);
		// print_hash( c_left_->_hash);
	}else{
        buffer.append(32, 0x00);

		// print_hash(h);
		mask ^= 0x10;
	}
	// cout << endl;
	
    buffer.append(get_key());

	if( c_right_ != NULL){
        buffer.append((char *) c_right_->get_hash(),32);
		// print_hash( c_right_->_hash);
	}else{
        buffer.append(32, 0x00);

		// print_hash(h);
		mask ^= 0x01;
	}
	// cout << endl;
	// cout << "Mask: " << hex << setw(2) << setfill('0') << (uint)mask << endl;
	
	
    buffer.append(1, mask);

    sse::crypto::Hash::hash(buffer, out);
    memcpy(hash_, out.data(), 32);

	// print_hash(_hash); cout << "\n\n" << endl;
	
}

void VBSTNode::refresh_hashes()
{
	if( c_left_ != NULL){
		c_left_->refresh_hashes();
	}
	
	if( c_right_ != NULL){
		c_right_->refresh_hashes();
	}
	
	
	__recompute_hash();
}

VBSTNode* VBSTNode::clone() const
{
    VBSTNode *n = new VBSTNode(get_key());
    return n;
}

void VBSTNode::copy_content(const VBSTNode *n)
{
    key_ = n->get_key();
}

unsigned int VBSTNode::tree_depth() const
{
	unsigned int d = 0;
	if ( c_left_ != NULL)
	{
		d = max(d, c_left_->tree_depth());
	}
	if ( c_right_ != NULL)
	{
		d = max(d, c_right_->tree_depth());
	}

	return d+1;
}

size_t VBSTNode::inner_node_count() const
{
	size_t c = 0;
	
	if ( c_left_ != NULL)
	{
		c +=  c_left_->inner_node_count();
	}
	if ( c_right_ != NULL)
	{
		c +=  c_right_->inner_node_count();
	}
	
	if( c_left_ != NULL &&  c_right_ != NULL)
	{
		c++;
	}
	return c;
}

size_t VBSTNode::leaves_count() const
{
	size_t c = 0;
	
	if ( c_left_ != NULL)
	{
		c +=  c_left_->leaves_count();
	}
	if ( c_right_ != NULL)
	{
		c +=  c_right_->leaves_count();
	}
	
	if( c_left_ == NULL &&  c_right_ == NULL)
	{
		c++;
	}
	return c;
}

size_t VBSTNode::one_child_node_count() const
{
	size_t c = 0;
	
	if ( c_left_ != NULL)
	{
		c +=  c_left_->one_child_node_count();
	}
	if ( c_right_ != NULL)
	{
		c +=  c_right_->one_child_node_count();
	}
	
	if( c_left_ != NULL &&  c_right_ == NULL)
	{
		c++;
	}
	if( c_left_ == NULL &&  c_right_ != NULL)
	{
		c++;
	}
	return c;
}


void VBSTNode::recompute_root_hash(const list<std::unique_ptr<VBSTAuthElement> > &auth, const unsigned char last_hash [32], unsigned char *new_root_hash)
{
	unsigned char current_hash [32];	
	
	if(last_hash != NULL){
//		cout << "Last hash not null\n";
		memcpy(new_root_hash,last_hash,32);
	}else{
//		cout << "Last hash IS NULL\n";
		memset(new_root_hash,0,32);
	}
	
	// cout << "Starting root hash, last hash = \n";
	// print_hash(new_root_hash);
	// cout << endl;
	
	for(auto it = auth.rbegin(); it != auth.rend(); ++it)
	{
		memcpy(current_hash,new_root_hash,32);
		
//		VBSTAuthElement cur = *it;
        
        (*it)->compute_hash(current_hash,new_root_hash);
		
		 // cout << "Updated root hash\n";
		 // print_hash(new_root_hash);
		 // cout << endl;
		
	}

}
void VBSTNode::recompute_root_hash_insertion(list<std::unique_ptr<VBSTAuthElement> > &auth, const string &key, unsigned char *new_root_hash)
{
	unsigned char current_hash [32];
    std::string buffer, out;
	memset(current_hash,0,32);

    // create a dummy authentication element and compute its hash
    VBSTAuthElement auth_elt(key, NULL, false, 0x11);
    auth_elt.compute_hash(current_hash, current_hash);
    	
	auto elt = auth.rbegin();

	if((*elt)->selected_branch){
		(*elt)->mask &= 0x01;
	}else{
		(*elt)->mask &= 0x10;
	}

	recompute_root_hash(auth, current_hash,new_root_hash);
}

void VBSTNode::check_authentication(const unsigned char *root_hash, const list<std::unique_ptr<VBSTAuthElement> > &auth, const unsigned char *last_hash)
{
	unsigned char current_hash [32];

	recompute_root_hash(auth, last_hash, current_hash);
	
	if (char_cmp(current_hash,root_hash,32) != 0)
	{
		string c_h_string((char *)current_hash, 32);
		string r_h_string((char *)root_hash, 32);
		
		IPUserInfoType info = 
			{
				{RecomputedHash, c_h_string},
				{ReferenceHash, r_h_string}				
			};
		
		throw InvalidProofException(IPNonMatchingRootHashes, "Hashes don't match.", info);
	}
}

void VBSTNode::non_membership_ladder(const string& key, const string& small_node_key, const string& large_node_key, const list<std::unique_ptr<VBSTAuthElement> > &auth)
{
    if(large_node_key.empty() && small_node_key.empty())
    {
		throw InvalidProofException(IPEmptyTree, "Tree has to be non empty.");
	}
	// first check that the keys are actually correctly ordered
	if(!small_node_key.empty() && small_node_key >= key) // small_node >= key
	{
		IPUserInfoType info = 
			{
				{SmallKey, small_node_key},
				{VerifiedKey, key}
			};
			
		throw InvalidProofException(IPInvalidSmallKey, "Misordered keys. Smaller key is bigger than the searched key.", info);
	}
		
		
    if(!large_node_key.empty() && large_node_key <= key) // small_node >= key
	{
        IPUserInfoType info =
			{
				{LargeKey, large_node_key},
				{VerifiedKey, key}
			};
			
		throw InvalidProofException(IPInvalidLargeKey, "Misordered keys. Larger key is smaller than the searched key.", info);
	}
	
	// the first element of the authentication list must be one of small_node or large_node
	VBSTAuthElement cur = *auth.back();
	bool direction;
		
	if(cur.mask == 0x00){
		throw InvalidProofException(IPInvalidLastNode, "Last node must have 1 child at most.");
	}
	
	if(!small_node_key.empty() && cur.key == small_node_key)
	{
		if(cur.selected_branch == LEFT) // left branch selected
		{
			throw InvalidProofException(IPInvalidLastBranch, "Invalid branch for last element.");
		}
		direction = LEFT;
	}else if(!large_node_key.empty() && cur.key == large_node_key)
	{
		if(cur.selected_branch == RIGHT) // right branch selected
		{
			throw InvalidProofException(IPInvalidLastBranch, "Invalid branch for last element.");
		}
		direction = RIGHT;
		
	}else{
		throw InvalidProofException(IPLastNodeNotMatching, "The first node in the proof is neither the larger nor the smaller.");
	}
	
	// if only one node exists, we are done here
    if(large_node_key.empty() || small_node_key.empty())
	{
		return;
	}
			
	auto it = auth.rbegin();
	it++;
	bool found_top_node = false;
	
		
	// first reach the other node (larger or smaller)
	for( ; it != auth.rend(); ++it)
	{
		cur = **it;
		
		if(direction == LEFT){ // we had the small node, search for the large node
			if(cur.key == large_node_key){
				found_top_node = true;
				break;
			}
		}
		if(direction == RIGHT){ // we had the large node, search for the small node
			if(cur.key == small_node_key){
				found_top_node = true;
				break;
			}
		}
		
		if(cur.selected_branch == direction)
		{
			throw InvalidProofException(IPInvalidBranch, "Unauthorized direction change when climbing up the tree.");
		}
	}
	
	if(!found_top_node){
		throw InvalidProofException(IPTopNodeNotFound, "Did not find the top node in the ladder.");
	}
}

void VBSTNode::check_proof(const string& key, bool found, const unsigned char *root_hash,  const string& small_node_key, const string& large_node_key, const list<std::unique_ptr<VBSTAuthElement> > &auth, const unsigned char *last_hash)
{
	if(found)
	{
		if(auth.back()->key != key)
		{
			IPUserInfoType info = 
				{
					{NodeKey, auth.back()->key},
					{VerifiedKey, key}
				};
			
			throw InvalidProofException(IPNonMatchingKeys, "Key of the first element of the membership proof does not match the verified key.", info);
		}
		VBSTNode::check_authentication(root_hash,auth, last_hash);
	}else{
		VBSTNode::check_authentication(root_hash,auth, last_hash);
		VBSTNode::non_membership_ladder(key, small_node_key, large_node_key, auth);
	}
}



void VBSTNode::check_insertion_proof(const string &key, const unsigned char *old_root_hash, const string &small_node_key, const string &large_node_key, const list<std::unique_ptr<VBSTAuthElement> > &auth)
{
	VBSTNode::check_authentication(old_root_hash,auth, NULL);
	VBSTNode::non_membership_ladder(key, small_node_key, large_node_key, auth);
}

VBSTNode* VBSTNode::leftmost_node(VBSTNode **previous)
{
	if( c_left_ == NULL)
	{
		return this;
	}
	
	if(previous != NULL)
	{
		*previous = this;
	}
	return  c_left_->leftmost_node(previous);
}

VBSTNode* VBSTNode::rightmost_node(VBSTNode **previous)
{
	if( c_right_ == NULL)
	{
		return this;
	}
	
	if(previous != NULL)
	{
		*previous = this;
	}
	return  c_right_->rightmost_node(previous);
}

VBSTNode* VBSTNode::delete_node(const string &key)
{
    int cmp = get_key().compare(key);

	VBSTNode  *new_node;
	
	if(cmp < 0){
		assert( c_left_ != NULL);
		if( c_left_ == NULL){
			cout << "Error: deletion of an unexisting node" << endl;
			return NULL;
		}
		
		// cout << "Deleting node at left" << endl;
		new_node =  c_left_->delete_node(key);
		 c_left_ = new_node;
		
		// if( c_left_ == NULL)
			// cout << "New node is NULL\n";
		
		__recompute_hash();
			
		return this;

		// del_node =  c_left_;
		// is_left = true;
	}else if(cmp > 0){ 
		assert( c_right_ != NULL);
		
		if( c_right_ == NULL){
			cout << "Error: deletion of an unexisting node" << endl;
			return NULL;
		}
		// the node to be deleted is on the right branch
		
		// cout << "Deleting node at right" << endl;
		
		new_node =  c_right_->delete_node(key);
		 c_right_ = new_node;
		
		// if( c_left_ == NULL)
			// cout << "New node is NULL";
		
		__recompute_hash();
		
		return this;
	}
	
	// cmp == 0
	
	// cout << "\n\n\nDELETING\n\n\n\n" << endl;
	unsigned char mask = get_mask();
	if(mask == 0x11) // node is a leaf
	{
		// just delete the node
		delete this;

		return NULL;
	}
	
	if(mask == 0x10) // only has a right node
	{
		// return the right node our child an delete ourself
		// be careful to set to NULL the right node (otherwise the former siblings will be destructed too)
		VBSTNode *next =  c_right_;
		
		 c_right_ = NULL;
		delete this;
		
		return next;	
	} 

	if(mask == 0x01) // only has a left node
	{
		// do the same as before but with del_node's left node instead
		VBSTNode *next =  c_left_;
		
		 c_left_ = NULL;
		delete this;
		
		return next;	
	} 
	
	// mask = 0x00, we has both left and right nodes
	// we have the node with key immediately following/precedingkey_,
	// and replace del_node by this node
	
	VBSTNode *rep_node, *prev_node;
	prev_node = this;
	
	rep_node = this-> c_left_->rightmost_node(&prev_node);
	// rep_node = this->leftmost_node(&prev_node);
	
    string rep_key = rep_node->get_key();
    
    const VBSTNode *copy_node = rep_node->clone();
    
	
	delete_node(rep_key);
	
    copy_content(copy_node);
	
    delete copy_node;
    
	// cout << "Recompute hash of modified node\n";
	__recompute_hash();
	
	return this;
}

bool VBSTNode::delete_node_authenticate(const string& key, VBSTNode** new_node, string& small_node_key, string& large_node_key, list<std::unique_ptr<VBSTAuthElement> > &auth_to_key,  unsigned char key_last_hash[32],  list<std::unique_ptr<VBSTAuthElement> > &aux_auth, unsigned char last_hash[32])
{
	VBSTNode_const_ptr small_node, large_node;
	bool found;
	const unsigned char* tmp;
		
	found = aux_search_node_authenticate(key, &small_node, &large_node, auth_to_key, &tmp);
	if(tmp != NULL){
		memcpy(key_last_hash,tmp,32);
	}else{
		memset(key_last_hash,0,32);
	}
	
	if(small_node != NULL){
		small_node_key = small_node->get_key();
	}else{
        small_node_key.clear();
	}
	
	if(large_node != NULL){
        large_node_key = large_node->get_key();
	}else{
        large_node_key.clear();
	}

	if(!found)
	{
		*new_node = this;
		return false;
	}

    assert(small_node != NULL);
    
	unsigned char mask = small_node->get_mask();

	if(mask == 0x11) // no child
	{
		// cout << "No child\n";
        memset(last_hash,0,32);
        aux_auth = list<std::unique_ptr<VBSTAuthElement> >();
        
	}else if(mask == 0x01 || mask == 0x10){ // one child...
		VBSTNode *next;
			
        aux_auth = list<std::unique_ptr<VBSTAuthElement> >();

		if(mask == 0x01) // ... left
		{
			next = small_node-> c_left_;
            aux_auth.push_back(std::unique_ptr<VBSTAuthElement>(small_node->create_auth_element(RIGHT)));
		}else{
			next = small_node-> c_right_;
            aux_auth.push_back(std::unique_ptr<VBSTAuthElement>(small_node->create_auth_element(LEFT)));
        }
		// cout << "Next node key: "; print_key(next->get_key());
		// cout << endl;
		// unsigned char const* left_hash;
		
        aux_auth.push_back(std::unique_ptr<VBSTAuthElement>(next->create_auth_element(LEFT)));

		if(next-> c_right_ == NULL)
		{
			memset(last_hash,0,32);
		}else{
			memcpy(last_hash,next-> c_right_->get_hash(),32);
		}
		
		
	}else{
	// mask = 0x00, we has both left and right nodes

		VBSTNode *rep_node;
		rep_node = small_node-> c_left_->rightmost_node(NULL);

		small_node->search_node_authenticate(rep_node->get_key(),small_node_key,large_node_key, aux_auth, last_hash);
	}

	*new_node = delete_node(key);

	return true;
}

void VBSTNode::check_deletion_proof(const string &key,
                                    bool found, const unsigned char *old_root_hash,
                                    const string &small_node_key, const string &large_node_key,
                                    const list<std::unique_ptr<VBSTAuthElement> > &auth_to_key, const unsigned char *key_last_hash,
                                    const list<std::unique_ptr<VBSTAuthElement> > &aux_auth, const unsigned char *last_hash)
{
	// check that the found flag is correct, as well as the (auth_to_key, key_last_hash) proof

	check_proof(key, found, old_root_hash, small_node_key, large_node_key, auth_to_key, key_last_hash);

	if(!found){	
		return;
	}

    if (auth_to_key.back()->mask != 0x11)
    {
		// if the remove node has two children, the auxiliary branch as to have 2 elements at least : 
		// the removed node itself the right most sibling of its left child
        if(aux_auth.size() >= 2) {
            unsigned char intermediate_root_hash [32];
            auth_to_key.back()->compute_hash(key_last_hash, intermediate_root_hash);
            string rep_node_key = aux_auth.back()->key;
            check_proof(rep_node_key, true, intermediate_root_hash, rep_node_key, rep_node_key, aux_auth, last_hash);
        }else{
			throw InvalidProofException(IPInvalidBranchLength, "Length of the auxiliary branch is incompatible with the removed node's mask.");
        }
    }
}

void VBSTNode::recompute_root_hash_deletion(const string &small_node_key, const string &large_node_key, list<std::unique_ptr<VBSTAuthElement> > &auth_to_key, const unsigned char *key_last_hash,  list<std::unique_ptr<VBSTAuthElement> > &aux_auth, const unsigned char *last_hash, unsigned char *new_root_hash)
{
	// the node has been found and should be deleted
	// the mask's node should be found in the auth_to_key list as the mask of the last element
	unsigned char mask = auth_to_key.back()->mask;
	
    list<std::unique_ptr<VBSTAuthElement> > calculation_ladder(std::move(auth_to_key));

	if(mask == 0x11) // node is a leaf
	{
		// cout << "Leaf\n";
		// remove the last element of the ladder (the removed element)
		// and update the mask of the previous one
		calculation_ladder.pop_back();
		
		if(calculation_ladder.back()->selected_branch){
			calculation_ladder.back()->mask |= 0x10;
		}else{
			calculation_ladder.back()->mask |= 0x01;
		}
	}else if(mask == 0x01 || mask == 0x10)
	{
		// cout << "One child\n";
		
		// remove the last element of the calculation_ladder (the removed node) and append the aux_auth list
		calculation_ladder.pop_back();
//        calculation_ladder.splice(calculation_ladder.end(), aux_auth);
        auto begin = aux_auth.begin();
        auto end = aux_auth.end();
        begin++;
        calculation_ladder.splice(calculation_ladder.end(), aux_auth, begin,end);

    }else{
		// cout << "Two children\n";
		
		// replace the key of the last element of the calculation_ladder by the one of the last element of the aux_auth list, append aux_auth and remove the last element
		// or equivalently append all but the last element of aux_auth
		// we also have to update some masks
		
		
        calculation_ladder.back()->copy_content(aux_auth.back().get());
        
		// aux_auth contains the element to be removed + the replacement node, its size is >= 2
		if(aux_auth.size() > 2){ // at least an other element
			auto begin = aux_auth.begin();
			auto end = aux_auth.end();
			begin++;end--;

            calculation_ladder.splice(calculation_ladder.end(), aux_auth, begin,end);
            
            if (aux_auth.back()->mask == 0x11) {
                // the replacement node is a leaf
                // when moved, it will be replaced by an empty node
                calculation_ladder.back()->mask |= 0x01;
            }else{
                // the mask of the replacement node father is unchanged
            }
		}else{
			// cout << "Replacement node is the left node\n";
            // recall that the replacement node is the rightmost node among the left siblings of the removed node
            // said otherwise, it is the smallest node of the set of node larger than key.
            // as a consequence, in this case, the replacement node had no right child but will once it replaced the removed node
            
            // update the mask
            calculation_ladder.back()->mask = aux_auth.back()->mask & 0x10;
		}
	}
	
	
	
	recompute_root_hash(calculation_ladder, last_hash, new_root_hash);
}

} // namespace verifiable_container
} // namespace sse

