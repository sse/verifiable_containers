#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE CRYPTO

#include <boost/test/unit_test.hpp>

#include <sse/crypto/random.hpp>

#include "src/vbst.hpp"
#include "src/valued_vbst.hpp"
#include "src/verifiable_set.hpp"
#include "src/verifiable_map.hpp"

#include <iostream>
#include <iomanip>

#include <vector>
#include <string>
#include <map>
#include <utility>
#include <unordered_set>
#include <array>
#include <random>
#include <algorithm>    // std::find
#include <chrono>

using namespace std;
using namespace sse::verifiable_containers;



string random_string(random_device &engine, size_t length)
{
	return sse::crypto::random_string(length);
}

vector<string> append_random_elements(random_device &engine, vector<string> &vec, size_t n, size_t key_length, size_t display_step = 1000)
{	
	string prompt = "\rCreating a random vector ... ";
    // cout << "Creating a random vector ...";
    for(size_t j = 0; j < n;)
    {
        cout << prompt << j << "/"<< n << flush;
        for(size_t i = 0; i < display_step && j < n; ++i, ++j )
        {
            vec.push_back(random_string(engine, key_length));
        }
    }
    cout << prompt << "Done                " << flush;
	cout << endl;
		
	return vec;
}

vector<string> create_random_vector(random_device &engine, size_t n, size_t key_length, size_t display_step = 1000)
{
	vector<string> vec;
	append_random_elements(engine, vec, n, key_length, display_step);
	return vec;
}

void append_random_pairs(random_device &engine, vector<pair<string, string>> &vec, size_t n, size_t first_length, size_t second_length, size_t display_step = 1000)
{
	
	string prompt = "\rCreating a random vector of pairs ... ";
    // cout << "Creating a random vector ...";
    for(size_t j = 0; j < n;)
    {
        cout << prompt << j << "/"<< n << flush;
        for(size_t i = 0; i < display_step && j < n; ++i, ++j )
        {
            vec.push_back(make_pair(random_string(engine, first_length), random_string(engine, second_length)));
        }
    }
    cout << prompt << "Done                " << endl;
	
}

vector<pair<string, string>> create_random_pair_vector(random_device &engine, size_t n, size_t first_length, size_t second_length, size_t display_step = 1000)
{
	vector<pair<string, string>> vec;
	append_random_pairs(engine, vec, n, first_length, second_length, display_step);
	return vec;
}

verifiable_set<string>* set_membership_check(random_device &engine, size_t set_size, vector<string> &elements, size_t key_length, size_t membership_test, size_t non_member_test)
{
	cout << "Verifiable set (non) membership tests: \n";
	cout << "Set size: " << set_size << " elements\n";
	cout << "Keys length: " << key_length << " bytes\n";
	cout << membership_test << " membership tests\n";
	cout << non_member_test << " non membership tests\n";
	
	
    
	size_t display_step = 1000;
	
	size_t elts_size = elements.size();
	
	if(elts_size < set_size){
		append_random_elements(engine, elements, set_size-elts_size, key_length, display_step);
    }
	
	
	cout << "Constructing structure ...";
    verifiable_set<string> *vset = new verifiable_set<string>(elements.begin(), elements.end());
    cout << " Done\n";
	
    auto digest = vset->digest();
	uint32_t r;
	bool alright;
    size_t get_time = 0, verif_time = 0;
	
	string prompt = "\rMembership tests ... ";
	
    for(size_t i = 0; i < membership_test;)
	{
		cout << prompt << i << "/"<< membership_test << flush;
		for(size_t j = 0; j < display_step && i < membership_test ; ++j, ++i)
		{
	        r = engine();
	        string s_key = elements[(r%elements.size())];
        
	        auto begin = std::chrono::high_resolution_clock::now();
	        auto proof = vset->contains(s_key);
	        auto end = std::chrono::high_resolution_clock::now();
        
	        get_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
		
			BOOST_REQUIRE(proof.found);
		        
	        begin = std::chrono::high_resolution_clock::now();
	        alright = verifiable_set<string>::check_membership_proof(digest , s_key, proof);
	        end = std::chrono::high_resolution_clock::now();
        
	        verif_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
		
			BOOST_REQUIRE(alright);	
		}
	}
    cout << prompt << "OK                " << endl;

#ifdef BENCHMARK
    cout << "Average time per successful search: ";
    cout << ((double)get_time)/(1e6*membership_test) << " ms" << endl;
    cout << "Average time per verification: ";
    cout << ((double)verif_time)/(1e6*membership_test) << " ms" << endl;
#endif
	
	prompt = "\rNon membership tests ... ";
	
	get_time = 0; verif_time = 0;
	
	
    for(size_t i = 0; i < non_member_test;)
	{
		cout << prompt << i << "/"<< non_member_test << flush;
		for(size_t j = 0; j < display_step && i < non_member_test ; ++j, ++i)
		{
	        string s_key = random_string(engine, key_length);
        
	        auto begin = std::chrono::high_resolution_clock::now();
	        auto proof = vset->contains(s_key);
	        auto end = std::chrono::high_resolution_clock::now();
        
	        get_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();

			if(proof.found)
			{
	            vector<string>::iterator it;
	            it = find (elements.begin(), elements.end(), s_key);
				BOOST_REQUIRE(it != elements.end());
			}
		        
	        begin = std::chrono::high_resolution_clock::now();
	        alright = verifiable_set<string>::check_membership_proof(digest , s_key, proof);
	        end = std::chrono::high_resolution_clock::now();
	    
		    verif_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
        
			BOOST_REQUIRE(alright);	
		}
	}
    cout << prompt << "OK                " << endl;

#ifdef BENCHMARK
    cout << "Average time per unsuccessful search: ";
    cout << ((double)get_time)/(1e6*membership_test) << " ms" << endl;
    cout << "Average time per verification: ";
    cout << ((double)verif_time)/(1e6*membership_test) << " ms" << endl;
#endif
	
	return vset;
}


void set_insertion_test(verifiable_set<string> *vset, vector<string> &elements, random_device &engine, size_t key_length, size_t insertion_count)
{
	cout << "Verifiable set insertion tests with " << insertion_count << " new elements\n";
	
	size_t display_step = 1000;
	vector<string> inserted_elements = create_random_vector(engine, insertion_count, key_length, display_step);
    
	elements.insert(elements.end(), inserted_elements.begin(), inserted_elements.end());
	
    auto digest = vset->digest();
    size_t insertion_time = 0, verif_time = 0;
    
	string prompt = "\rInsertion tests ... ";
    for(size_t i = 0; i < insertion_count;)
	{
		cout << prompt << i << "/"<< insertion_count << flush;
		for(size_t j = 0; j < display_step && i < insertion_count ; ++j, ++i)
		{
	        auto begin = std::chrono::high_resolution_clock::now();
	        auto proof = vset->add(inserted_elements[i]);
	        auto end = std::chrono::high_resolution_clock::now();
			
	        insertion_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
			
	        begin = std::chrono::high_resolution_clock::now();
	        digest = verifiable_set<string>::check_update_addition(digest, inserted_elements[i], proof);
	        end = std::chrono::high_resolution_clock::now();
        
	        verif_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
		
			BOOST_REQUIRE(digest == vset->digest());	
    	}
	}
    cout << prompt << "OK                " << endl;
#ifdef BENCHMARK
    cout << "Average time per insertion: ";
    cout << ((double)insertion_time)/(1e6*insertion_count) << " ms" << endl;
    cout << "Average time per verification: ";
    cout << ((double)verif_time)/(1e6*insertion_count) << " ms" << endl;
#endif
}


void set_deletion_test(verifiable_set<string> *vset,  vector<string> &elements, random_device &engine, size_t deletion_count)
{
	cout << "Verifiable set deletion tests: removing " << deletion_count << " elements\n";

	size_t display_step = 1000;
    auto digest = vset->digest();

    uint32_t r;

	string prompt = "\rDeletion tests ... ";
    size_t deletion_time = 0, verif_time = 0;
	
    for(size_t i = 0; i < deletion_count;)
	{
		cout << prompt << i << "/"<< deletion_count << flush;
		for(size_t j = 0; j < display_step && i < deletion_count ; ++j, ++i)
		{
	        r = engine();
			r = r%elements.size();
			
	        string s_key = elements[r];

	        auto begin = std::chrono::high_resolution_clock::now();
	        auto proof = vset->remove(s_key);
	        auto end = std::chrono::high_resolution_clock::now();
			
	        deletion_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();

	        begin = std::chrono::high_resolution_clock::now();
	        digest = verifiable_set<string>::check_update_deletion(digest, s_key, proof);
	        end = std::chrono::high_resolution_clock::now();
        
	        verif_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
			

			BOOST_REQUIRE(digest == vset->digest());	
			
//	        elements.erase(elements.begin()+r);
		}
    }
    cout << prompt << "OK                " << endl;
#ifdef BENCHMARK
    cout << "Average time per deletion: ";
    cout << ((double)deletion_time)/(1e6*deletion_count) << " ms" << endl;
    cout << "Average time per verification: ";
    cout << ((double)verif_time)/(1e6*deletion_count) << " ms" << endl;
#endif
}


verifiable_map<string, string>* map_lookup_check(random_device &engine, size_t map_size, vector<pair<string, string>> &elements, size_t key_length, size_t value_length, size_t membership_test, size_t non_member_test)
{

	cout << "Verifiable map lookup tests: \n";
	cout << "Map size: " << map_size << " elements\n";
	cout << "Keys length: " << key_length << " bytes\n";
	cout << "Values length: " << value_length << " bytes\n";
	cout << membership_test << " membership tests\n";
	cout << non_member_test << " non membership tests\n";
	
	
    
	size_t display_step = 1000;
	
	size_t elts_size = elements.size();
	
	if(elts_size < map_size){
		append_random_pairs(engine, elements, map_size-elts_size, key_length, display_step);
    }
	
	cout << "Constructing structure ...";
    verifiable_map<string, string> *vht = new verifiable_map<string,string>(elements.begin(), elements.end());
    cout << " Done\n";

	
	bool alright = true;
	
	// test random inserted elements
	

	string prompt = "\rLookup tests ... ";
	uint32_t r;
    auto digest = vht->digest();
    size_t get_time = 0, verif_time = 0;
	
    for(size_t i = 0; i < membership_test;)
	{
		cout << prompt << i << "/"<< membership_test << flush;
		for(size_t j = 0; j < display_step && i < membership_test ; ++j, ++i)
		{
			r = engine();
			r = r%elements.size();
			
			string s_key = elements[r].first;
			string s_value = elements[r].second;

	        std::unique_ptr<string> v;
        
	        auto begin = std::chrono::high_resolution_clock::now();
	        auto proof = vht->get(s_key);
			auto end = std::chrono::high_resolution_clock::now();
			
	        get_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
			
			
			BOOST_REQUIRE(proof.found);
			
	        begin = std::chrono::high_resolution_clock::now();
	        alright = verifiable_map<string, string>::check_lookup_proof(digest , s_key, proof);
			end = std::chrono::high_resolution_clock::now();  
			
	        verif_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
			
			BOOST_REQUIRE(alright);	
		}
	}
    cout << prompt << "OK                " << endl;
#ifdef BENCHMARK
    cout << "Average time per successful lookup: ";
    cout << ((double)get_time)/(1e6*membership_test) << " ms" << endl;
    cout << "Average time per verification: ";
    cout << ((double)verif_time)/(1e6*membership_test) << " ms" << endl;
#endif
		
	prompt = "\rEmpty lookup tests ... ";
    get_time = 0; verif_time = 0;
	
    for(size_t i = 0; i < non_member_test;)
	{
		cout << prompt << i << "/"<< non_member_test << flush;
		for(size_t j = 0; j < display_step && i < non_member_test ; ++j, ++i)
		{
	        string s_key = random_string(engine, key_length);
			
	        auto begin = std::chrono::high_resolution_clock::now();
	        auto proof = vht->get(s_key);
			auto end = std::chrono::high_resolution_clock::now();  
			
	        get_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
			        
	        begin = std::chrono::high_resolution_clock::now();
	        alright = verifiable_map<string, string>::check_lookup_proof(digest , s_key, proof);
			end = std::chrono::high_resolution_clock::now();  
        
	        verif_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
		
			BOOST_REQUIRE(alright);	
			
		}
	}
    cout << prompt << "OK                " << endl;
#ifdef BENCHMARK
    cout << "Average time per unsuccessful lookup: ";
    cout << ((double)get_time)/(1e6*membership_test) << " ms" << endl;
    cout << "Average time per verification: ";
    cout << ((double)verif_time)/(1e6*membership_test) << " ms" << endl;
#endif
			
	return vht;
}

void map_addition_check(verifiable_map<string, string> *vht, vector<pair<string, string>> &elements, random_device &engine, size_t key_length, size_t value_length, size_t insertion_count, size_t replacement_count)
{
	cout << "Verifiable map insertion tests with " << insertion_count << " new elements\n";
	cout << "and " << replacement_count << " replacements\n";
	
	size_t display_step = 1000;
	
	vector<pair<string, string>> inserted_elements = create_random_pair_vector(engine, insertion_count, key_length, value_length, display_step);
	
	elements.insert(elements.end(), inserted_elements.begin(), inserted_elements.end());
	    
    auto digest = vht->digest();
    
	string prompt = "\rInsertion tests ... ";
    size_t insertion_time = 0, verif_time = 0;

    for(size_t i = 0; i < insertion_count;)
	{
		cout << prompt << i << "/"<< insertion_count << flush;
		for(size_t j = 0; j < display_step && i < insertion_count ; ++j, ++i)
		{
			
	        auto begin = std::chrono::high_resolution_clock::now();
	        auto proof = vht->set(inserted_elements[i].first, inserted_elements[i].second);
			auto end = std::chrono::high_resolution_clock::now();  
			
	        insertion_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
			
			
	        begin = std::chrono::high_resolution_clock::now();
			digest = verifiable_map<string, string>::check_update_addition(digest, inserted_elements[i].first, inserted_elements[i].second, proof);
			end = std::chrono::high_resolution_clock::now();  
			
			verif_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
			
			BOOST_REQUIRE(digest == vht->digest());	
		}
    }
    cout << prompt << "OK                " << endl;
#ifdef BENCHMARK
    cout << "Average time per insertion: ";
    cout << ((double)insertion_time)/(1e6*insertion_count) << " ms" << endl;
    cout << "Average time per verification: ";
    cout << ((double)verif_time)/(1e6*insertion_count) << " ms" << endl;
#endif

	prompt = "\rReplacement tests ... ";
	insertion_time = 0; verif_time = 0;
    uint32_t index;

    auto replacements = create_random_vector(engine, replacement_count, value_length);

    for(size_t i = 0; i < replacement_count;)
	{
		cout << prompt << i << "/"<< replacement_count << flush;
		for(size_t j = 0; j < display_step && i < replacement_count ; ++j, ++i)
		{
	        index = engine()%elements.size();
	
	        auto begin = std::chrono::high_resolution_clock::now();
	        auto proof = vht->set(elements[index].first, replacements[i]);
			auto end = std::chrono::high_resolution_clock::now();  
			
	        insertion_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();

			
	        begin = std::chrono::high_resolution_clock::now();
	        digest = verifiable_map<string, string>::check_update_addition(digest, elements[index].first, replacements[i], proof);
			end = std::chrono::high_resolution_clock::now();  
			
			verif_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();

			BOOST_REQUIRE(digest == vht->digest());	
		}
    }
    cout << prompt << "OK                " << endl;
#ifdef BENCHMARK
    cout << "Average time per replacement: ";
    cout << ((double)insertion_time)/(1e6*replacement_count) << " ms" << endl;
    cout << "Average time per verification: ";
    cout << ((double)verif_time)/(1e6*replacement_count) << " ms" << endl;
#endif
}

BOOST_AUTO_TEST_CASE(vset)
{
	cout << "\n\n=== Verifiable Set ====" << endl;
	random_device engine;
	size_t key_length = 16;
	vector<string> elements;

	verifiable_set<string> *vset = set_membership_check(engine, 1e4, elements, key_length, 1e4, 1e4);

	set_insertion_test(vset, elements, engine, key_length, 1e4);
	set_deletion_test(vset, elements, engine, 1e4);

	delete vset;
}

BOOST_AUTO_TEST_CASE(vht)
{
	cout << "\n\n=== Verifiable Map ====" << endl;
	random_device engine;
	size_t key_length = 16;
	size_t value_length = 16;
	vector<pair<string, string>> elements;
	
	verifiable_map<string, string> *vht = map_lookup_check(engine, 1e4, elements, key_length, value_length, 1e4, 1e4);
    map_addition_check(vht, elements, engine, key_length, value_length, 1e4, 1e4);

	delete vht;
}
