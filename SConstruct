import os

# PREFIX = "prefix"
# AddOption('--prefix',dest='prefix',type='string', nargs=1, default='install',
    # action='store', metavar='DIR', help='installation prefix')

env = Environment()
# def_env = DefaultEnvironment(PREFIX = GetOption('prefix'))

try:
    env.Append(ENV = {'TERM' : os.environ['TERM']}) # Keep our nice terminal environment (like colors ...)
except:
    print "Not running in a terminal"

sse_root_dir = Dir('#/..').srcnode().abspath

config = {}
config['sse_root_dir'] = Dir('#/..').srcnode().abspath
config['cryto_lib_dir'] = config['sse_root_dir'] + "/crypto/library"
config['cryto_include'] = config['cryto_lib_dir']  + "/include"
config['cryto_lib'] = config['cryto_lib_dir']  + "/lib"

if FindFile('config.scons', '.'):
    SConscript('config.scons', exports=['env','config'])

env.Append(CCFLAGS = ['-fPIC','-Wall', '-march=native'])
env.Append(CXXFLAGS = ['-std=c++11'])
env.Append(CPPPATH = [config['cryto_include']])
env.Append(LIBPATH = [config['cryto_lib']])
env.Append(RPATH = [config['cryto_lib']])

#Workaround for OS X
if env['PLATFORM'] == 'darwin':
    rpathprefix = '-rpath'
    env.Append(LINKFLAGS = [[rpathprefix, lib] for lib in env['RPATH']])
    # env.Append(LINKFLAGS = ['-rpath', cryto_lib_dir+'/lib'])



env.Append(LIBS = ['crypto', 'sse_crypto'])
env['AS'] = ['yasm']
env.Append(ASFLAGS = ['-D', 'LINUX'])

env['STATIC_AND_SHARED_OBJECTS_ARE_THE_SAME']=1


if env['PLATFORM'] == 'darwin':
    env.Append(ASFLAGS = ['-f', 'macho64'])
else:
    env.Append(ASFLAGS = ['-f', 'x64', '-f', 'elf64'])


debug = ARGUMENTS.get('debug', 0)
if int(debug):
    env.Append(CCFLAGS = ['-g','-O'])
else:
	env.Append(CCFLAGS = ['-O2'])


def run_test(target, source, env):
    app = str(source[0].abspath)
    if os.spawnl(os.P_WAIT, app, app)==0:
        return 0
    else:
        return 1

bld = Builder(action = run_test)
env.Append(BUILDERS = {'Test' :  bld})


objects = SConscript('src/build.scons', exports='env', variant_dir='build', duplicate=0)
Clean(objects, 'build')

test_set = env.Program('bin/test_set',['test_set.cpp'] + objects)
test_table = env.Program('bin/test_table',['test_table.cpp'] + objects)

shared_lib_env = env.Clone();

if env['PLATFORM'] == 'darwin':
    # We have to add '@rpath' to the library install name
    shared_lib_env.Append(LINKFLAGS = ['-install_name', '@rpath/libverifiable_containers.dylib'])

library_build_prefix = 'library'
shared_lib = shared_lib_env.SharedLibrary(library_build_prefix+'/lib/verifiable_containers',objects)
static_lib = env.StaticLibrary(library_build_prefix+'/lib/verifiable_containers',objects)

headers = Glob('src/*.h') + Glob('src/*.hpp')
headers_lib = [env.Install(library_build_prefix+'/include/sse/verifiable_containers', headers)]

env.Clean(headers_lib,[library_build_prefix+'/include'])

Alias('headers', headers_lib)
# Alias('lib', [shared_lib, static_lib] + headers_lib)
Alias('lib', [static_lib, shared_lib] + headers_lib)



check_env = env.Clone()

benchmark = ARGUMENTS.get('benchmark', 0)
if int(benchmark):
    check_env.Append(CPPDEFINES = ['BENCHMARK'])

# Use a temporary environment to search for boost unit test framework
# On OS X, use of -rpath prevents Scons to find it if we directly use env or check_env

tmp_env = env.Clone()

if not check_env.GetOption('clean'):
    conf = Configure(tmp_env)
    if conf.CheckLib('boost_unit_test_framework'):
        print 'Found boost unit test framework'

        check_env.Append(LIBS = ['boost_unit_test_framework'])

        test_prog = check_env.Program('check', ['checks.cpp'] + objects)

        test_run = check_env.Test('test_run', test_prog)
        Depends(test_run, test_prog)

        check_env.Alias('check', [test_prog, test_run])

    else:
        print 'boost unit test framework not found'
        print 'Skipping checks. Be careful!'
    tmp_env = conf.Finish()

check_env.Clean('check', ['check'] + objects)

env.Default(test_table, test_set)