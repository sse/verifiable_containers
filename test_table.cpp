//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>
#include <iomanip>

#include <vector>
#include <string>
#include <map>
#include <utility>
#include <unordered_set>
#include <array>
#include <random>
#include <algorithm>    // std::find
#include <chrono>

#include "src/vbst.hpp"
#include "src/valued_vbst.hpp"
#include "src/verifiable_map.hpp"

using namespace std;
using namespace sse::verifiable_containers;


void test_membership(VBSTValuedNode<string> *root,const string &key)
{
    string small_node_key, large_node_key;
	list<std::unique_ptr<VBSTAuthElement> > auth;
	unsigned char last_hash[32] = {0x00};
	
	bool found;
    std::unique_ptr<string> v;
	found = root->search_node_authenticate(key, v, small_node_key, large_node_key, auth, last_hash);

	try{
		VBSTNode::check_proof(key, found, root->get_hash(), small_node_key, large_node_key, auth, last_hash);
	}catch(exception const& e){
		 cerr << "ERROR when checking search proof: " << e.what() << endl;
	}
    
    if (found) {
        cout << "Found value: " << *v << endl;
    }else{
        cout << "Key not found" << endl;
    }
}



void create_random_set(VBSTValuedNode<string> **root, vector<string> &s, size_t n, size_t display_step = 1000)
{
    srand ((uint)time(NULL));

	random_device engine;
	
	unsigned char key[16];
	
	size_t step = display_step;
	// cout << "\n";
	for(size_t j = 0; j < n/step; ++j)
	{
		for(size_t i = 0; i < step; ++i)
		{
			// int r = rand();	
			uint_fast32_t r;
			for(size_t l = 0; l < 4; ++l)
			{
				r = engine();
				key[4*l] = (unsigned char) r;
				key[4*l+1] = (unsigned char) (r>>8);
				key[4*l+2] = (unsigned char) (r>>16);
				key[4*l+3] = (unsigned char) (r>>24);
			}	
			string s_key((char*)key,16);
            string value(s_key.begin()+1, s_key.end()-1); // take a substring as value


			s.push_back(s_key);
				
			if(*root == NULL)
			{
				*root = new VBSTValuedNode<string>(s_key, value);
			}else{
				(*root)->insert_node_key_value(s_key, value);
			}
		}
		cout << "\r" << j*step << "/"<< n << flush;
		
	}
	for(size_t i = 0; i < n%step; ++i)
	{
		// int r = rand();		
		uint_fast32_t r;
		for(size_t l = 0; l < 4; ++l)
		{
			r = engine();
			key[4*l] = (unsigned char) r;
			key[4*l+1] = (unsigned char) (r>>8);
			key[4*l+2] = (unsigned char) (r>>16);
			key[4*l+3] = (unsigned char) (r>>24);
		}	
		string s_key((char*)key,16);
        string value(s_key.begin()+1, s_key.end()-1); // take a substring as value

		s.push_back(s_key);
				
		if(*root == NULL)
		{
			*root = new VBSTValuedNode<string>(s_key, value);
		}else{
			(*root)->insert_node_key_value(s_key, value);
		}
	}	
	cout << "\r" << n << "/"<< n << endl;
}

void create_random_vector(vector<string> &s, size_t n, size_t display_step = 1000)
{
    srand ((uint)time(NULL));
    
    random_device engine;
    
    unsigned char key[16];
    
    size_t step = display_step;
    // cout << "\n";
    for(size_t j = 0; j < n/step; ++j)
    {
        for(size_t i = 0; i < step; ++i)
        {
            // int r = rand();
            uint_fast32_t r;
            for(size_t l = 0; l < 4; ++l)
            {
                r = engine();
                key[4*l] = (unsigned char) r;
                key[4*l+1] = (unsigned char) (r>>8);
                key[4*l+2] = (unsigned char) (r>>16);
                key[4*l+3] = (unsigned char) (r>>24);
                
            }
            string s_key((char*)key,16);
            
            
            s.push_back(s_key);
        }
        cout << "\r" << j*step << "/"<< n << flush;
        
    }
    for(size_t i = 0; i < n%step; ++i)
    {
        // int r = rand();
        uint_fast32_t r;
        for(size_t l = 0; l < 4; ++l)
        {
            r = engine();
            key[4*l] = (unsigned char) r;
            key[4*l+1] = (unsigned char) (r>>8);
            key[4*l+2] = (unsigned char) (r>>16);
            key[4*l+3] = (unsigned char) (r>>24);
        }
        string s_key((char*)key,16);
        
        s.push_back(s_key);
    }
    cout << "\r" << n << "/"<< n << endl;
}

void create_random_map(map<string, string> &m, size_t n, size_t display_step = 1000)
{
    srand ((uint)time(NULL));
    
    random_device engine;
    
    unsigned char key[16];
    unsigned char value[16];
    
    size_t step = display_step;
    // cout << "\n";
    for(size_t j = 0; j < n/step; ++j)
    {
        for(size_t i = 0; i < step; ++i)
        {
            uint32_t r;
            for(size_t l = 0; l < 4; ++l)
            {
                r = engine();
                key[4*l] = (unsigned char) r;
                key[4*l+1] = (unsigned char) (r>>8);
                key[4*l+2] = (unsigned char) (r>>16);
                key[4*l+3] = (unsigned char) (r>>24);
                
            }

            for(size_t l = 0; l < 4; ++l)
            {
                r = engine();
                value[4*l] = (unsigned char) r;
                value[4*l+1] = (unsigned char) (r>>8);
                value[4*l+2] = (unsigned char) (r>>16);
                value[4*l+3] = (unsigned char) (r>>24);
                
            }
            
            string s_key((char*)key,16);
            string s_value((char*)value,16);
            
            
            m[s_key] = s_value;
        }
        cout << "\r" << j*step << "/"<< n << flush;
        
    }
    for(size_t i = 0; i < n%step; ++i)
    {
        uint32_t r;
        for(size_t l = 0; l < 4; ++l)
        {
            r = engine();
            key[4*l] = (unsigned char) r;
            key[4*l+1] = (unsigned char) (r>>8);
            key[4*l+2] = (unsigned char) (r>>16);
            key[4*l+3] = (unsigned char) (r>>24);
            
        }
        
        for(size_t l = 0; l < 4; ++l)
        {
            r = engine();
            value[4*l] = (unsigned char) r;
            value[4*l+1] = (unsigned char) (r>>8);
            value[4*l+2] = (unsigned char) (r>>16);
            value[4*l+3] = (unsigned char) (r>>24);
            
        }
        
        string s_key((char*)key,16);
        string s_value((char*)value,16);
        
        
        m[s_key] = s_value;
    }
    cout << "\r" << n << "/"<< n << endl;
}

void create_random_pair_vec(vector<pair<string, string>> &v, size_t n, size_t display_step = 1000)
{
    srand ((uint)time(NULL));
    
    random_device engine;
    
    unsigned char key[16];
    unsigned char value[16];
    
    size_t step = display_step;
    // cout << "\n";
    for(size_t j = 0; j < n/step; ++j)
    {
        for(size_t i = 0; i < step; ++i)
        {
            uint32_t r;
            for(size_t l = 0; l < 4; ++l)
            {
                r = engine();
                key[4*l] = (unsigned char) r;
                key[4*l+1] = (unsigned char) (r>>8);
                key[4*l+2] = (unsigned char) (r>>16);
                key[4*l+3] = (unsigned char) (r>>24);
                
            }
            
            for(size_t l = 0; l < 4; ++l)
            {
                r = engine();
                value[4*l] = (unsigned char) r;
                value[4*l+1] = (unsigned char) (r>>8);
                value[4*l+2] = (unsigned char) (r>>16);
                value[4*l+3] = (unsigned char) (r>>24);
                
            }
            
            string s_key((char*)key,16);
            string s_value((char*)value,16);
            
            
            v.push_back(make_pair(s_key, s_value));
        }
        cout << "\r" << j*step << "/"<< n << flush;
        
    }
    for(size_t i = 0; i < n%step; ++i)
    {
        uint32_t r;
        for(size_t l = 0; l < 4; ++l)
        {
            r = engine();
            key[4*l] = (unsigned char) r;
            key[4*l+1] = (unsigned char) (r>>8);
            key[4*l+2] = (unsigned char) (r>>16);
            key[4*l+3] = (unsigned char) (r>>24);
            
        }
        
        for(size_t l = 0; l < 4; ++l)
        {
            r = engine();
            value[4*l] = (unsigned char) r;
            value[4*l+1] = (unsigned char) (r>>8);
            value[4*l+2] = (unsigned char) (r>>16);
            value[4*l+3] = (unsigned char) (r>>24);
            
        }
        
        string s_key((char*)key,16);
        string s_value((char*)value,16);
        
        
        v.push_back(make_pair(s_key, s_value));
    }
    cout << "\r" << n << "/"<< n << endl;
}


void small_test()
{
	std::vector<pair<unsigned char, string>> in;
	
	
	in.push_back(make_pair(0x24, "__24__"));

	in.push_back(make_pair(0x21, "__21__"));
	in.push_back(make_pair(0x03, "__3__"));
	in.push_back(make_pair(0x15, "__15__"));
	in.push_back(make_pair(0x25, "__25__"));
    in.push_back(make_pair(0x23, "__23__"));
	in.push_back(make_pair(0x28, "__28__"));
	in.push_back(make_pair(0x06, "__6__"));

	VBSTValuedNode<string> *root = new VBSTValuedNode<string>(string(1,in[0].first), in[0].second);

	for (size_t i = 1; i < in.size(); ++i)
	{
        if(i == in.size()-1)
            cout << "\n\n\n\n\n\n\n\nInsert last \n";
        else
            cout << "Insert\n";
        
        VBSTValuedNode<string> *n = new VBSTValuedNode<string>(string(1,in[i].first), in[i].second);

		root->insert_node(n);
	}

	cout << "Tree depth: " << root->tree_depth() << endl;

	VBSTNode_const_ptr result = NULL;

    string k(1,0x05);
	result = root->search_node(k);

	if (result == NULL)
	{
		cout << "Did not find key " << k << endl;
	}else{
		cout << "Found key " << k << endl;
	}

	test_membership(root, k);

	delete root;
}

void random_test(size_t n = 1000, size_t membership_test = 50, size_t non_member_test = 50)
{
	VBSTValuedNode<string> *root = NULL;
	vector<string> s;
	
	random_device engine;
	
	uint_fast32_t r;
	
	srand((uint)time(NULL)); // for small tests only
	
	cout << "Started building the set \n";
	create_random_set(&root, s, n);
	cout << "Finished building the set \n";
	
	cout << "Content: \n";
	
	// for(size_t i = 0; i < s.size(); ++i)
	// {
	// 	print_key((unsigned char*)s[i].data()); cout << endl;
	// }
	
	cout << "Tree depth " << root->tree_depth() << endl;
	cout << "Leaves count " << root->leaves_count() << endl;
	cout << "Inner Nodes count " << root->inner_node_count() << endl;
	cout << "One child nodes count " << root->one_child_node_count() << endl;
	
	bool alright = true;
	
	// test random inserted elements
	
	size_t elapsed_search = 0, elapsed_verify = 0;
	
	cout << "\n===TESTS===\n";
	cout << "Started " << membership_test << " membership tests\n";
	
	for(size_t i = 0; i < membership_test; ++i)
	{
		r = engine();
		string s_key = s[(r%s.size())];
        string expected_value(s_key.begin()+1, s_key.end()-1); // take a substring as value

        std::unique_ptr<string> v;
        
        string small_node_key, large_node_key;
		unsigned char last_hash[32] = {0x00};
		
		list<std::unique_ptr<VBSTAuthElement> > auth;
		bool found;
	
		auto begin = std::chrono::high_resolution_clock::now();

		found = root->search_node_authenticate(s_key, v, small_node_key, large_node_key, auth, last_hash);
				
		auto end = std::chrono::high_resolution_clock::now();
		
		elapsed_search += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
	
		begin = std::chrono::high_resolution_clock::now();
		
		try{
			VBSTNode::check_proof(s_key, found, root->get_hash(), small_node_key, large_node_key, auth, last_hash);
		}catch(exception const& e){
			 cerr << "ERROR when checking membership proof: " << e.what() << endl;
	 		 alright = false;
		}
		
		
		end = std::chrono::high_resolution_clock::now();
		
		elapsed_verify += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();

		
		if(!found){
			cout << "Error: inserted element not found \n";
			alright = false;
        }else{
            if (expected_value != *v) {
                cout << "Error: invalid value \n";
                alright = false;
            }
        }
		
	}
	cout << "Finished membership tests\n";
	cout << "Average time per successful search: ";
	cout << ((double)elapsed_search)/(1e6*membership_test) << " ms" << endl;
	cout << "Average time per successful verification: ";
	cout << ((double)elapsed_verify)/(1e6*membership_test) << " ms" << endl;
	
	cout << "Started " << non_member_test << " non membership tests\n";
	elapsed_search = 0, elapsed_verify = 0;
	
	for(size_t i = 0; i < non_member_test; ++i)
	{
		
		unsigned char key[16];
		
		for(size_t l = 0; l < 4; ++l)
		{
			r = engine();
			key[4*l] = (unsigned char) r;
			key[4*l+1] = (unsigned char) (r>>8);
			key[4*l+2] = (unsigned char) (r>>16);
			key[4*l+3] = (unsigned char) (r>>24);
		}	
		
        string s_key((char*)key,16);
        std::unique_ptr<string> v;

        string small_node_key, large_node_key;
		unsigned char last_hash[32] = {0x00};
		list<std::unique_ptr<VBSTAuthElement> > auth;
		bool found;
		
		auto begin = std::chrono::high_resolution_clock::now();

		found = root->search_node_authenticate(s_key, v, small_node_key, large_node_key, auth, last_hash);
		
		auto end = std::chrono::high_resolution_clock::now();
		
		elapsed_search += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
	
		begin = std::chrono::high_resolution_clock::now();
		
		try{
			VBSTNode::check_proof(s_key, found, root->get_hash(), small_node_key, large_node_key, auth, last_hash);
		}catch(exception const& e){
			cerr << "ERROR when checking membership proof: " << e.what() << endl;
			
			cout << "Check failed on key: ";
			print_key(key);
			cout << endl;
	 		 
			alright = false;
		}
		
		end = std::chrono::high_resolution_clock::now();
		
		elapsed_verify += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
		
		if(found){
			// check that the element is actually in the tree
			string s_key((char*)key,16);
			
		    vector<string>::iterator it;
			it = find (s.begin(), s.end(), s_key);
		    if (it == s.end()) // not found
			{
				cout << "Error: non inserted element found \n";
				alright = false;
			}
		}
	}
	
	cout << "Finished non membership tests\n";
	cout << "Average time per unsuccessful search: ";
	cout << ((double)elapsed_search)/(1e6*non_member_test) << " ms" << endl;
	cout << "Average time per unsuccessful verification: ";
	cout << ((double)elapsed_verify)/(1e6*non_member_test) << " ms" << endl;
	
	if(alright){
		cout << "Everything happened correctly!\n";
	}else{
		cout << "A problem was encountered\n";
	}
	
	delete root;
	
}

void test_insertion()
{
	std::vector<unsigned char> in;
	
	
	in.push_back(0x24);

	in.push_back(0x21);
	in.push_back(0x3);
	in.push_back(0x15);
	in.push_back(0x25);
	in.push_back(0x23);
	in.push_back(0x28);
	in.push_back(0x6);

    VBSTValuedNode<string> *root = new VBSTValuedNode<string>(string(1,in[0]), string(16,in[0]));

	for (size_t i = 1; i < in.size(); ++i)
	{
		// cout << "\n\n\n\nInsertion: \n";
		root->insert_node_key_value(string(1,in[i]), string(16,in[i]));
	}
	
    string k(1,0x6);
    string v(1,0x00);
	
	unsigned char old_root[32], new_root[32];
	memcpy(old_root,root->get_hash(),32);
	
	cout << "Old root hash: \n";
	print_hash(old_root);
	cout << endl;
	
    string small_node_key, large_node_key;
	list<std::unique_ptr<VBSTAuthElement> > auth;
    bool already_existed;
    unsigned char last_hash[32];
	
	// root->insert_node_authenticate(k,small_node_key, large_node_key, auth, already_existed, last_hash);
	
    VBSTValuedNode<string> *n = new VBSTValuedNode<string>(k,v);
    root->insert_node_authenticate(n, small_node_key, large_node_key, auth, already_existed, last_hash);
	
	// cout << "Last hash: " << last_hash << endl;
	// print_hash(last_hash);
	
	// cout << "\n\n\n\n\n";
	
	try{
        if (!already_existed) {
            VBSTNode::check_insertion_proof(k, old_root, small_node_key, large_node_key, auth);
			
            // VBSTNode::recompute_root_hash_insertion(auth, k, new_root);
        }else{
            cout << "\n\nAlready existed\n" << endl;
            VBSTNode::check_proof(k, true, old_root, small_node_key, large_node_key, auth, last_hash);
            // memcpy(new_root,old_root,32);
        }
		cout << "RECOMPUTE HASH\n";
        VBSTValuedNode<string>::recompute_root_hash_insertion(auth, k, v, already_existed, last_hash, new_root);
		
	}catch(exception const& e){
		cout << "ERROR when checking insertion proof: " << e.what() << endl;
	
		cout << "Check failed on key: " << k << endl;
	}

	
	cout << "\nNew root hash: \n";
	print_hash(new_root);
		
	cout << "\nReal root hash: \n";
	print_hash(root->get_hash());
	cout << endl;
	
		
	delete root;
}


void test_deletion()
{
    std::vector<pair<unsigned char, string>> in;
    
    
    in.push_back(make_pair(0x24, "__24__"));
    
    in.push_back(make_pair(0x21, "__21__"));
    in.push_back(make_pair(0x03, "__3__"));
    in.push_back(make_pair(0x15, "__15__"));
    in.push_back(make_pair(0x25, "__25__"));
    in.push_back(make_pair(0x23, "__23__"));
    in.push_back(make_pair(0x28, "__28__"));
    in.push_back(make_pair(0x06, "__6__"));
    in.push_back(make_pair(0x22, "__22__"));

    VBSTValuedNode<string> *root = new VBSTValuedNode<string>(string(1,in[0].first), in[0].second);

	for (size_t i = 1; i < in.size(); ++i)
	{
		root->insert_node_key_value(string(1,in[i].first), in[i].second);
	}
	
    string k(1,0x21);
	
	
	
	bool found;
    string small_node_key, large_node_key;
	
	list<std::unique_ptr<VBSTAuthElement> > auth_to_key;
 	unsigned char key_last_hash[32] = {0x00};
 	unsigned char last_hash[32] = {0x00};
	list<std::unique_ptr<VBSTAuthElement> > aux_auth;
	unsigned char new_root_hash[32];
		
		
	unsigned char old_root[32];
	memcpy(old_root,root->get_hash(),32);
	
	
	cout << "Old root hash: \n";
	print_hash(old_root);
	cout << endl;
	
	VBSTNode_const_ptr result = NULL;
	
	
	// cout << "=== TEST MEMBERSHIP ===\n";
	// test_membership(root, k);

	result = root->search_node(k);
	if (result == NULL)
	{
		cout << "Did not find key " << hex << (int)k[0] << endl;
	}else{
		cout << "Found key " << hex << (int)k[0] << endl;
	}
	
	// cout << "\n\n\n\n\n\n\n\nDeletion\n";
	
	// root = root->delete_node(k);
	cout << "=== DELETE ===\n";
	
    VBSTNode *temp;
	found = root->delete_node_authenticate(k, &temp, small_node_key, large_node_key, auth_to_key,  key_last_hash,  aux_auth, last_hash);
    root = dynamic_cast<VBSTValuedNode<string>*>(temp);
    
	// cout << "\n\n\n";
	result = root->search_node(k);

	if (result == NULL)
	{
		cout << "Did not find key " << (int)k[0] << endl;
	}else{
		cout << "Found key " << (int)k[0] << endl;
	}
	
	cout << "=== CHECK ===\n";
	try{
        VBSTNode::check_deletion_proof(k, found, old_root, small_node_key, large_node_key, auth_to_key, key_last_hash, aux_auth, last_hash);
        cout << "Proof passed!\n";
        if(found){
            VBSTNode::recompute_root_hash_deletion(small_node_key, large_node_key, auth_to_key, key_last_hash, aux_auth, last_hash, new_root_hash);
        }else{
            memcpy(new_root_hash, old_root, 32);
        }

        
		cout << "\nNew root hash: \n";
		print_hash(new_root_hash);
		cout << endl;
	
		cout << "\nReal root hash: \n";
		print_hash(root->get_hash());
		cout << endl;
	
		
	}catch(exception const& e){
		cerr << "ERROR when checking deletion proof: " << e.what() << endl;

		cout << "Check failed on key: " << k << endl;
	}
	
		
	delete root;
}

int main()
{
	cout << "=== TEST VHT IMPLEMENTATION ===" << endl;
	
//	 random_test(1e4, 1e4, 1e4);
//	 small_test();
//	 test_insertion();
//	test_deletion();

	return 0;
}
